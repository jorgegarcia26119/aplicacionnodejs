import conexion from "./conexion.js";

var alumnosDB={};
alumnosDB.insertar =function insertar(alumno){
    return new Promise((resolve,rejects)=>{
        let sqlConsulta="insert into alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){
     
            if(err){
                console.log("Surgio un error (alumnos.js)"+err.message);
                rejects(err);
            }
            else{
                const alumnoI={
                    id:res.id                  
                };
                resolve(alumnoI);
            }
        });
    });
};
alumnosDB.mostrarTodos = function mostrarTodos(){
    return new Promise((resolve,reject)=>{
        let sqlConsulta="select * from alumnos";
        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                console.log("Surgio un error (alumnos.js): ",err);
                rejects(err);
            }
            else{
                resolve(res);
            }
        })
    })
};
export default alumnosDB;
