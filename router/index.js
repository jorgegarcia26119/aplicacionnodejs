import express from 'express'; 
import json from 'body-parser';
import alumnosDB from '../models/alumnos.js';
import conexion from '../models/conexion.js';

export const router = express.Router();

router.get('/',(req,res)=>{
    res.render('index',{titulo:"Mis Practicas js",nombre:"Garcia Gonzalez Jorge Enrique"})
})
//enique
//TABLA
router.get('/tabla',(req,res)=>{
    const params={
        numero:req.query.numero
    }
    res.render('tabla',params);
})
router.post('/tabla',(req,res)=>{
    const params={
        numero:req.body.numero
    }
    res.render('tabla',params);
})

//COTIZACION
router.get('/cotizacion', (req, res) => { 
    const params = {  folio: '',
    valor: 0,
    pinicial: 0,
    plazo: 0,
    pagoInicial: 0,
    totalAFin: 0,
    pagoMensual: 0};

    res.render('cotizacion', params);
})
router.post('/cotizacion',(req,res)=>{
    const folio = req.body.folio;
    const valorAutomovil = parseFloat(req.body.valor);
    const porcentajeInicial = parseFloat(req.body.pinicial);
    const plazo = parseInt(req.body.plazo);
    
    // Realizar cálculos necesarios
    const pagoInicial = (valorAutomovil * porcentajeInicial) / 100;
    const totalAFin = valorAutomovil - pagoInicial;
    const pagoMensual = totalAFin / plazo;

    const params = {
        folio: folio,
        valor: valorAutomovil,
        pinicial: porcentajeInicial,
        plazo: plazo,
        pagoInicial: pagoInicial.toFixed(2),
        totalAFin: totalAFin.toFixed(2),
        pagoMensual: pagoMensual.toFixed(2)
    };

    res.render('cotizacion',params);
})
//ALUMNOS
let rows;
router.get('/alumnos',async(req,res)=>{
    rows = await alumnosDB.mostrarTodos();
    res.render('alumnos',{reg:rows});
})

router.post('/alumnos', async (req, res) => {
    // Parámetros
    let params;
    try {
        params = {
            // id: req.body.id,
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad,
        }
        const res = await alumnosDB.insertar(params);
    }
    catch(err) {
        console.error(err);
        res.status(400).send(`Sucedió un error: ${err}`)
    }
    const rows = await alumnosDB.mostrarTodos();
    res.render('alumnos', {
        reg: rows,
    });   
});

export default {router}
